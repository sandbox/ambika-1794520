(function ($) {
    Drupal.behaviors.reveal_integration = {
        attach: function(context, settings) {
            var revealSettings = settings.revealSettings;
            Reveal.initialize({
                // Display controls in the bottom right corner
                controls: (revealSettings.controls == 0) ? false : true,

                // Display a presentation progress bar
                progress: (revealSettings.progress == 0) ? false : true,

                // Push each slide change to the browser history
                history: false,

                // Enable keyboard shortcuts for navigation
                keyboard: (revealSettings.keyboard == 0) ? false : true,

                // Loop the presentation
                loop: (revealSettings.loop == 0) ? false : true,

                // Number of milliseconds between automatically proceeding to the
                // next slide, disabled when set to 0
                autoSlide: (revealSettings.autoslide != null) ? parseInt(revealSettings.autoslide) : 0,

                // Enable slide navigation via mouse wheel
                mouseWheel: (revealSettings.mouseWheel == 0) ? false : true,

                // Apply a 3D roll to links on hover
                rollingLinks: true,

                // Transition style
                transition: (revealSettings.transition != null) ? revealSettings.transition : 'default'
                // default/cube/page/concave/linear(2d)
            });
        }
    };
})(jQuery);
