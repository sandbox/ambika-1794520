<?php
/**
 * @file
 * page--views-reveal-view.tpl.php
 * Example of a page reveal template
 * Render only page content to optimize presentation design.
 */

?>
<?php print render($page['content']); ?>
