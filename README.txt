
Views Reveal
===============

The Views Reveal module is a Views Style Plugin that can be used to output
Views in a Reveal presentation. This module uses the JS plugin Reveal, made by 
Hakim El Hattab. 

JS plugin demo : http://lab.hakim.se/reveal-js/#/

Requirements
============

Views 3 is required for this module to be of any use. 
You also need the reveal.js plugin. Get the Drupal version here : 
https://github.com/AmbikaFR/reveal.js.git

This version is a fork of the original plugin version. 

Description
===========

This module will create a View type of Reveal that will display nodes in a
Reveal presentation.

Settings are available for keyboard controls, theme, transitions effects, 
and more.

Installation
============

1/ make a "git clone" of https://github.com/AmbikaFR/reveal.js.git, in your 
libraries folder [sites/all/libraries]

2/ install the Reveal module as usual

3/ you can copy page--views-reveal-view.tpl.php in your theme folder. 
    This template is called for all Reveal Views, as page template. 
    In your theme folder, you can customize the template. 

Create a Reveal view
====================

1- Create a view with the "Reveal" display

2- Choose the "Reveal" plugin. In the settings, choose your Reveal settings : 
    visibility of controls, keyboard control, mousewheel control.

3- Set your view as usual

Vertical Reveal views
=====================

To add vertical slides, create a field to group your vertical slides. 
In views plugin settings, choose your group field. That's all !
