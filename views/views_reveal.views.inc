<?php
/**
 * @file
 * views_reveal.views.inc
 * Views include file for Views Reveal module
 */

/**
 * Implements hook_views_plugins().
 */
function views_reveal_views_plugins() {
  return array(
    'display' => array(
      'reveal' => array(
        'title' => t('Reveal'),
        'help' => t('Display the view as a reveal presentation, with a URL and menu links.'),
        'handler' => 'views_plugin_display_page',
        'theme' => 'views_view_reveal_display',
        'uses hook menu' => TRUE,
        'contextual links locations' => array('page'),
        'use ajax' => FALSE,
        'use pager' => FALSE,
        'use more' => FALSE,
        'accept attachments' => TRUE,
        'admin' => t('Reveal'),
        'help topic' => 'display-reveal',
      ),
    ),
    'style' => array(
      'reveal' => array(
        'title' => t('Reveal presentation'),
        'help' => t('Displays rows as Reveal HTML5 presentation'),
        'handler' => 'views_plugin_style_reveal',
        'theme' => 'views_view_reveal',
        'uses row plugin' => TRUE,
        'uses row class' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'help topic' => 'style-list',
      ),
    ),
  );
}
