<?php
/**
 * @file
 * Views style plugin for Views Reveal module
 */

/**
 * The views plugin style for Reveal Views
 */
class views_plugin_style_reveal extends views_plugin_style {
  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $themes = views_reveal_themes_list();
    $options = array();
    foreach ($themes as $key => $theme) {
      $options[$key] = $theme['name'];
    }
    $form['theme'] = array(
      '#title' => t('Theme'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->options['theme'],
    );
    $form['transition'] = array(
      '#title' => t('Transition type'),
      '#type' => 'select',
      '#options' => array(
        'default' => 'Default transition',
        'cube' => 'Cube',
        'page' => 'Page',
        'concave' => 'Concave',
        'linear(2d)' => 'Linear 2D',
      ),
      '#default_value' => $this->options['transition'],
    );
    $form['controls'] = array(
      '#title' => t('Display controls'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['controls'],
    );
    $form['progress'] = array(
      '#title' => t('Display progress bar'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['progress'],
    );
    $form['keyboard'] = array(
      '#title' => t('Activate keyboard navigation in slides'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['keyboard'],
    );
    $form['loop'] = array(
      '#title' => t('Play the slides in loop'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['loop'],
    );
    $form['mouseWheel'] = array(
      '#title' => t('Activate mouse wheel navigation in slides'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['mouseWheel'],
    );
    $form['autoslide'] = array(
      '#title' => t('Duration between each slides'),
      '#description' => 'Duration between each slide, in milliseconds. Type "0" to disable automatic scroll. ',
      '#type' => 'textfield',
      '#default_value' => (empty($this->options['autoslide'])) ? 0 : $this->options['autoslide'],
    );
  }

  /**
   * Render the grouping sets.
   *
   * Plugins may override this method if they wish some other way of handling
   * grouping.
   *
   * Add is_vertical information for Reveal plugin.
   *
   * @param Array $sets
   *   Array containing the grouping sets to render.
   * @param Integer $level
   *   Integer indicating the hierarchical level of the grouping.
   *
   * @return string
   *   Rendered output of given grouping sets.
   */
  function render_grouping_sets($sets, $level = 0) {
    $output = '';
    foreach ($sets as $set) {
      $row = reset($set['rows']);
      // Render as a grouping set.
      if (is_array($row) && isset($row['group'])) {
        $output .= theme(views_theme_functions('views_view_grouping', $this->view, $this->display),
          array(
            'view' => $this->view,
            'grouping' => $this->options['grouping'][$level],
            'grouping_level' => $level,
            'rows' => $set['rows'],
            'title' => $set['group'],
            'is_vertical' => $set['is_vertical'],
          )
        );
      }
      // Render as a record set.
      else {
        if ($this->uses_row_plugin()) {
          foreach ($set['rows'] as $index => $row) {
            $this->view->row_index = $index;
            $set['rows'][$index] = $this->row_plugin->render($row);
          }
        }
        $output .= theme($this->theme_functions(),
          array(
            'view' => $this->view,
            'options' => $this->options,
            'grouping_level' => $level,
            'rows' => $set['rows'],
            'title' => $set['group'],
            'is_vertical' => (isset($set['is_vertical'])) ? $set['is_vertical'] : FALSE,
          )
        );
      }
    }
    unset($this->view->row_index);
    return $output;
  }

  /**
   * Group records as needed for rendering.
   *
   * Add is_vertical information for Reveal plugin.
   *
   * @param Array $records
   *   An array of records from the view to group.
   * @param Array $groupings
   *   An array of grouping instructions on which fields to group. If empty, the
   *   result set will be given a single group with an empty string as a label.
   * @param Boolean $group_rendered
   *   Boolean value whether to use the rendered or the raw field value for
   *   grouping. If set to NULL the return is structured as before
   *   Views 7.x-3.0-rc2. After Views 7.x-3.0 this boolean is only used if
   *   $groupings is an old-style string or if the rendered option is missing
   *   for a grouping instruction.
   *
   * @return Array
   *   The grouped record set.
   *   A nested set structure is generated if multiple grouping fields are used.
   *
   *   @code
   *   array(
   *     'grouping_field_1:grouping_1' => array(
   *       'group' => 'grouping_field_1:content_1',
   *       'rows' => array(
   *         'grouping_field_2:grouping_a' => array(
   *           'group' => 'grouping_field_2:content_a',
   *           'rows' => array(
   *             $row_index_1 => $row_1,
   *             $row_index_2 => $row_2,
   *             // ...
   *           )
   *         ),
   *       ),
   *     ),
   *     'grouping_field_1:grouping_2' => array(
   *       // ...
   *     ),
   *   )
   *   @endcode
   */
  function render_grouping($records, $groupings = array(), $group_rendered = NULL) {
    // This is for backward compability, when $groupings was a string containing
    // the ID of a single field.
    if (is_string($groupings)) {
      $rendered = $group_rendered === NULL ? TRUE : $group_rendered;
      $groupings = array(array(
          'field' => $groupings,
          'rendered' => $rendered,
        ));
    }

    // Make sure fields are rendered.
    $this->render_fields($this->view->result);
    $sets = array();
    if ($groupings) {
      foreach ($records as $index => $row) {
        // Iterate through configured grouping fields to determine the
        // hierarchically positioned set where the current row belongs to.
        // While iterating, parent groups, that do not exist yet, are added.
        $set = &$sets;
        foreach ($groupings as $info) {
          $field = $info['field'];
          $rendered = isset($info['rendered']) ? $info['rendered'] : $group_rendered;
          $rendered_strip = isset($info['rendered_strip']) ? $info['rendered_strip'] : FALSE;
          $grouping = '';
          $group_content = '';
          // Group on the rendered version of the field, not the raw.  That way,
          // we can control any special formatting of the grouping field through
          // the admin or theme layer or anywhere else we'd like.
          if (isset($this->view->field[$field])) {
            $group_content = $this->get_field($index, $field);
            if ($this->view->field[$field]->options['label']) {
              $group_content = $this->view->field[$field]->options['label'] . ': ' . $group_content;
            }
            if ($rendered) {
              $grouping = $group_content;
              if ($rendered_strip) {
                $group_content = $grouping = strip_tags(htmlspecialchars_decode($group_content));
              }
            }
            else {
              $grouping = $this->get_field_value($index, $field);
              // Not all field handlers return a scalar value,
              // e.g. views_handler_field_field.
              if (!is_scalar($grouping)) {
                $grouping = md5(serialize($grouping));
              }
            }
          }

          // Create the group if it does not exist yet.
          if (empty($set[$grouping])) {
            $set[$grouping]['group'] = $group_content;
            $set[$grouping]['rows'] = array();
          }
          $set = &$set[$grouping]['rows'];
        }
        $set[$index] = $row;
        $group_index = $this->get_field($index, $field);
        $sets[$group_content]['is_vertical'] = (empty($group_index)) ?
            FALSE : TRUE;
      }
    }
    else {
      // Create a single group with an empty grouping field.
      $sets[''] = array(
        'group' => '',
        'rows' => $records,
      );
    }

    // If this parameter isn't explicitely set modify the output to be fully
    // backward compatible to code before Views 7.x-3.0-rc2.
    // @TODO Remove this as soon as possible e.g. October 2020
    if ($group_rendered === NULL) {
      $old_style_sets = array();
      foreach ($sets as $group) {
        $old_style_sets[$group['group']] = $group['rows'];
      }
      $sets = $old_style_sets;
    }
    return $sets;
  }
}
