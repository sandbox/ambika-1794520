<?php
/**
 * @file
 * views-view-reveal.tpl.php
 * Views Reveal rows group template.
 */

/**
 * Render a group of rows for Reveal plugin
 *
 * @param $rows
 *  An array of HTML rows rendered
 *
 * @param $is_vertical
 *  Boolean value. Is this group is a vertical group or not?
 */
?>
<?php if ($is_vertical) : ?>
  <section>
  <?php endif; ?>
  <?php foreach ($rows as $key => $row) : ?>
    <section><?php print $row; ?></section>
  <?php endforeach; ?>
  <?php if ($is_vertical) : ?>
  </section>
<?php endif; ?>
