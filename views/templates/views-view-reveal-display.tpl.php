<?php
/**
 * @file
 * views-view-reveal-display.tpl.php
 * Views Reveal display template.
 */

/**
 * Render the Reveal display
 *
 * @param $rows
 *  The rows content in HTML
 *
 * @param $empty
 *  An empty content
 */
?>
<div class="reveal">
  <div class="state-background"></div>
  <div class="slides">
    <?php if ($rows): ?>
      <?php print $rows; ?>
    <?php elseif ($empty): ?>
      <div class="view-empty">
        <?php print $empty; ?>
      </div>
    <?php endif; ?>
  </div>
  <aside class="controls">
    <a class="left" href="#">&#x25C4;</a>
    <a class="right" href="#">&#x25BA;</a>
    <a class="up" href="#">&#x25B2;</a>
    <a class="down" href="#">&#x25BC;</a>
  </aside>

  <div class="progress"><span></span></div>
</div>
